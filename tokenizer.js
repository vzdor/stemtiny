'use strict';

// https://github.com/NaturalNode/natural/tree/master/lib/natural/tokenizers

class Tokenizer {
  static tokenize(text) {
    return this.trim(text.split(/\W+/))
  }

  static trim(array) {
    while (array[array.length - 1] == '')
      array.pop();

    while (array[0] == '')
      array.shift();

    return array;
  }

}

module.exports = Tokenizer
