// For motivation, refer to index-bucket.txt.

'use strict';

let Index = require('./index')

class IndexBucket {
  // options.Bucket: something like a Bucket
  constructor(options) {
    this.Bucket = options.Bucket
    // A table of indexId -> loadIndex promise
    this._loadCache = {}
  }

  // Returns a Promise
  // .then(results)
  // results: {docId1: [token1, token2], docId2: [token2]}
  find(text) {
    let tokens = Index.tokenize(text)
    let promises = tokens.map((token) => this._find(token))
    return Promise.all(promises).then((results) => {
      return results.reduce(this._addResults.bind(this), {})
    })
  }

  // Returns a Promise
  //  then(indexes)
  //  indexes is an array of [indexId, index]
  addDoc(doc) {
    // NOTE: tokenize must return a unique array of tokens. We do not know
    // how the bucket works. It is not true that:
    //   (new Bucket('test')).next() == (new Bucket('test')).next()
    // If tokenize returns the same token twice, a token may end up
    // in different indexes, which is not what we want.
    let tokens = Index.tokenize(doc.text)
    let promises = tokens.map((token) => this._addDocid(token, doc.id))
    return Promise.all(promises)
  }

  // Momorize loaded index.
  // If in addDoc, two words map to the same index, there will be
  // two loadIndex calls.
  // Should return a Promise
  loadIndex(id) {
    return (this._loadCache[id] ||
           (this._loadCache[id] = this._loadIndex(id)))
  }

  // Implement
  // _loadIndex(id) {}

  // Abstract
  // indexes is an array of indexes.
  // Implement with a transaction if in parallel environment.
  saveIndex(indexes) {
  }

  _addDocid(token, docId) {
    let bucket = new this.Bucket(token)
    let add = (resolve, reject) => {
      let indexId = bucket.next()
      this.loadIndex(indexId).then((index) => {
        if (index.docs(token).length > 0 || bucket.length == 0) {
          index.addDocid(token, docId)
          resolve([indexId, index])
        } else
          add(resolve, reject)
      }).catch(reject)
    }
    return new Promise(add)
  }

  _addResults(results, array) {
    let token = array[0],
        docIds = array[1]
    Index.format(results, docIds, token)
    return results
  }

  _find(token) {
    let bucket = new this.Bucket(token)
    let find = (resolve, reject) => {
      let indexId = bucket.next()
      // console.log(`_find: ${indexId} ${token}`)
      this.loadIndex(indexId).then((index) => {
        let docIds = index.docs(token)
        if (docIds.length || bucket.length == 0) {
          resolve([token, docIds])
        } else if (bucket.length > 0) {
          find(resolve, reject)
        }
      }).catch(reject)
    }
    return new Promise(find)
  }
}

module.exports = IndexBucket
