'use strict';

/*
  Token document - Tokdoc, must have the following methods:
   addDocid(docId) -- adds a docId
   delDocid(docId)
   token -- getter and setter
   docIds -- array
   constructor({token})
   static findAll(tokens) -- returns a promise and resolves with token documents.
*/

const MyIndex = require('./my_index')

class AsyncIndex extends MyIndex {
  constructor({Tokdoc, tokdocs = []}) {
    super()
    this.Tokdoc = Tokdoc
    this._map = new Map(tokdocs)
  }

  // Returns a Promise.
  // Resolves with {docId1: [token1, token2], docId2: [token2]}.
  find(text) {
    let tokens = AsyncIndex.tokenize(text)
    return this._loadTokdocs(tokens).then(() => {
      return tokens.reduce(this._addResult.bind(this), {})
    })
  }

  // Returns an array of document ids
  docs(token) {
    let tokdoc = this._map.get(token)
    if (!tokdoc) return []
    return tokdoc.docIds
  }

  // TODO: same as Index addDoc.
  addDoc(doc) {
    let tokens = AsyncIndex.tokenize(doc.text)
    tokens.forEach((token) => this.addDocid(token, doc.id))
  }

  // Find or instantiate a token document and add docId to it.
  // It will not test if docId is already indexed.
  addDocid(token, docId) {
    let tokdoc = this._map.get(token)
    if (! tokdoc) {
      tokdoc = new this.Tokdoc({token: token})
      this._map.set(token, tokdoc)
    }
    tokdoc.addDocid(docId)
  }

  get tokdocs() {
    return Array.from(this._map.values())
  }

  // Load token documents.
  _loadTokdocs(tokens) {
    let missing = tokens.filter((token) => ! this._map.has(token))
    return this.Tokdoc.findAll(missing).then((tokdocs) => {
      tokdocs.forEach((tokdoc) => this._map.set(tokdoc.token, tokdoc))
    })
  }
}

module.exports = AsyncIndex
