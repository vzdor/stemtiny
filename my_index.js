'use strict';

let Tokenizer = require('./tokenizer')
let IrregularVerbs = require('./irregular_verbs')
let Stopwords = require('./stopwords')
let stemmer = require('./stemmer')

class MyIndex {
  // Returns token document ids
  // docs(token)  { implement }

  // text: text
  // results: unique tokens array
  static tokenize(text) {
    let tokens = Tokenizer.tokenize(text)
        .map((token) => token.toLowerCase())
        .filter((token) => ! Stopwords.includes(token))
        .map((token) => IrregularVerbs.get(token) || token)
        .map((token) => stemmer(token)) // to turn of stemmer(word, debug) debugging
    return [... new Set(tokens)]
  }

  // Input: token: [docIds]
  // Results: {docId1: [arrayOfTokens]}
  static format(results, docIds, token) {
    docIds.forEach((docId) => {
      let docTokens = results[docId]
      if (! docTokens)
        docTokens = results[docId] = []
      docTokens.push(token)
    })

  }

  _addResult(results, token) {
    let docIds = this.docs(token)
    MyIndex.format(results, docIds, token)
    return results
  }
}

module.exports = MyIndex
