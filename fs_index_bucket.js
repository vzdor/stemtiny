'use strict';

let fs = require('fs')
let path = require('path')

let Index = require('./index')
let Bucket = require('./bucket')
let IndexBucket = require('./index_bucket')

class FsIndexBucket extends IndexBucket {
  constructor(options = {}) {
    options.Bucket = options.Bucket || Bucket
    super(options)
    this.indexDir = options.indexDir
  }

  _loadIndex(id) {
    let filePath = path.resolve(this.indexDir, id)
    return new Promise((resolve, reject) => {
      // Resolve with a new index if file does not exist
      if (! fs.existsSync(filePath))
        return resolve(new Index())
      // If file exists
      fs.readFile(filePath, (err, text) => {
        if (err)
          reject(err)
        else {
          let index = new Index(JSON.parse(text))
          resolve(index)
        }
      })
    })
  }

}

module.exports = FsIndexBucket
