let FsIndexBucket = require('../fs_index_bucket')
let TfIdf = require('../tf_idf')

let idxBucket = new FsIndexBucket({indexDir: '../playground/indexes'})
let query = process.argv[2]

if (! query) {
  console.log('Query?')
  return
}

idxBucket.find(query).then((results) => {
  let tfIdf = new TfIdf(results)
  console.log(tfIdf.order())
  let indexes = idxBucket._loadCache // XXX not indexes
  let stats = {
    loadCount: Object.keys(indexes).length,
    keys: Object.keys(indexes).sort()
  }
  console.log(stats)
}).catch((e) => console.log(e))
