let fs = require('fs')
let path = require('path')

// corpus is an array: ["text1", "text2", ...]
let corpus = require('../playground/corpus')
let FsIndexBucket = require('../fs_index_bucket')

let idxBucket = new FsIndexBucket({indexDir: '../playground/indexes'})
let indexMap = new Map()

function done() {
  console.log('Writing index files...')
  indexMap.forEach((index, idxId) => {
    let text = JSON.stringify(index)
    fs.writeFileSync(path.join(idxBucket.indexDir, idxId), text)
  })
}

function load(i) {
  let text = corpus[i]
  idxBucket.addDoc({id: i, text: text}).then((indexPairs) => {
    indexPairs.forEach((ary) => {
      let idxId = ary[0],
          index = ary[1]
      indexMap.set(idxId, index)
    })
  }).then(() => {
    if (i < corpus.length - 1)
      load(i + 1)
    else
      done()
  }).catch((error) => console.log(error))
}

console.log('Bulding index...')
load(0)
