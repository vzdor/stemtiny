'use strict';


let MyIndex = require('./my_index')

class Index extends MyIndex {
  constructor(json) {
    super()
    this._map = new Map(json)
  }

  // Returns an object, ie. it is unsorted.
  // {docId1: [token1, token2], docId2: [token2]}
  find(text) {
    let tokens = Index.tokenize(text)
    return tokens.reduce(this._addResult.bind(this), {})
  }

  // Returns an array of document ids
  docs(token) {
    return this._map.get(token) || []
  }

  // doc: {id: 1, text: 'hello, bob'}
  addDoc(doc) {
    let tokens = Index.tokenize(doc.text)
    tokens.forEach((token) => this.addDocid(token, doc.id))
  }

  addDocid(token, docId) {
    let entry = this._map.get(token)
    if (! entry) {
      entry = []
      this._map.set(token, entry)
    }
    if (! entry.includes(docId))
      entry.push(docId)
  }

  delDocid(token, docId) {
    // TODO
  }

  delDoc(oldDoc) {
    // TODO
    let tokens = Index.tokenize(oldDoc.text)
    tokens.forEach((token) => this.delDocid(token, oldDoc.id))
  }

  updateDoc(oldDoc, newDoc) {
    // TODO
    delDoc(oldDoc)
    addDoc(newDoc)
  }

  toJSON() {
    return Array.from(this._map.entries())
  }
}

module.exports = Index
