'use strict';

const LetterIndexes = {
  t: 5,
  a: 4,
  s: 3,
  h: 3,
  w: 3,
  i: 3,
  o: 3,
  b: 3,
  m: 3,
  '*' : 2 // Default number of indexes
}

class Bucket extends Array {
  constructor(token, letterIndexes = LetterIndexes) {
    super()
    this.letterIndexes = letterIndexes
    this.letter = token[0]
    this._addIndexIds()
  }

  // POPs a random index id
  next() {
    let i = Math.floor(Math.random() * this.length)
    let indexId = this[i]
    this.splice(i, 1)
    return indexId
  }

  // Returns an array of indexes for the letter.
  _addIndexIds() {
    let indexCount = this.letterIndexes[this.letter] || this.letterIndexes['*']
    let keys = []
    for (let i = 0; i < indexCount; i++)
      this.push(`${this.letter}-${i}`)
  }
}

module.exports = Bucket
