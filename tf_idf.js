'use strict';

class TfIdf {
  // items: {docId1: [token1, token2], docId2: [token1]}
  constructor(items) {
    this._items = items
    this._docIds = Object.keys(items)
    this._idfCache = {}
  }

  // Returns a sorted items array:
  // [{id: docId, tokens: [..], score: 1.1}, ...]
  order() {
    let results = this._docIds.map(this._addScore.bind(this))
    return results.sort((a, b) => b.score - a.score)
  }

  _addScore(docId) {
    let result = {id: docId, tokens: this._items[docId]}
    let tf = this._tf(docId)
    result.score = result.tokens.reduce((acc, token) => {
      return acc + tf * this._idf(token)
    }, 0)
    return result
  }

  _tf(docId) {
    // (number of times the token accur in the document) / (number of tokens in the document)
    // I do not keep token count. (to reduce the index size)

    return 1
  }

  // returns:
  // (number of documents) / (number of documents with that token)
  _idf(token) {
    let idf = this._idfCache[token]
    if (idf === undefined) {
      idf = Math.log(this._docIds.length/this._docCount(token))
      this._idfCache[token] = idf
    }
    return idf
  }

  // returns the number of documents with that token
  _docCount(token) {
    // TODO: Count in a loop instead of making a new array and counting elements.
    return this._docIds.filter(
      (docId) => this._items[docId].includes(token)).length
  }
}

module.exports = TfIdf
