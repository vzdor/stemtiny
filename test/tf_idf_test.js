'use strict';

let assert = require('assert')
let TfIdf = require('../tf_idf')

describe('TfIdf', function() {
  describe('#order', function() {
    it('returns an array with scores', function() {
      let tfIdf = new TfIdf({1: ['you', 'i'], 2: ['bob']})
      let results = tfIdf.order()
      assert(Array.isArray(results))
      assert(results[0].score)
    })

    it('returns sorted array, rare first', function() {
      let items = {
        1: ['you', 'i'],
        2: ['you', 'he'],
        3: ['you', 'i', 'he'],
        4: ['fish'],
        5: ['you', 'i', 'he'],
      }
      let tfIdf = new TfIdf(items)
      let results = tfIdf.order()
      assert.equal(results[0].id, 4)
    })

    context('with similar items', function() {
      it('assigns equal score', function() {
        let items = {
          1: ['you', 'eat'],
          2: ['i', 'fish'],
        }
        let tfIdf = new TfIdf(items)
        let scores = tfIdf.order().map((r) => r.score)
        assert.equal(scores[0], scores[1])
      })
    })

  })

})
