'use strict';

let assert = require('assert')
let Index = require('../index')

describe('Index', function() {
  describe('#addDocid', function() {
    it('adds docid', function() {
      let idx = new Index()
      idx.addDocid('bob', 3)
      let results = idx.find('bob')
      assert.deepEqual(results, {3: ['bob']})
    })
  })

  describe('#docs', function() {
    it('returns an array of documents for the token', function() {
      let idx = new Index()
      idx.addDocid('bob', 3)
      idx.addDocid('bob', 7)
      idx.addDocid('blah', 9)
      assert.deepEqual(idx.docs('bob').sort(), [3, 7])
    })

    context('when token does not exist', function() {
      it('returns an empty array', function() {
        let idx = new Index()
        assert.deepEqual(idx.docs('bob'), [])
      })
    })
  })

  describe('#find', function() {
    let idx = new Index()
    let doc1 = {
      id: 1,
      text: 'Bob eat bananas.'
    }
    let doc2 = {
      id: 2,
      text: 'Jane ate an apple.'
    }

    beforeEach(function() {
      idx.addDoc(doc1)
      idx.addDoc(doc2)
    })

    it('returns relevant docId and tokens', function() {
      let results = idx.find('banana man')
      assert.deepEqual(results, {1: ['banana']})
    })

    it('ignores case', function() {
      let results = idx.find('bob')
      assert.deepEqual(results, {1: ['bob']})
    })

    it('converts irregular verbs', function() {
      assert.deepEqual(idx.find('eating'), {2: ['eat'], 1: ['eat']})
    })

    it('ignores stop words', function() {
      assert.deepEqual(idx.find('an'), {})
    })

    context('with an empty index', function() {
      it('returns empty object', function() {
        let idx = new Index()
        let results = idx.find("blah")
        assert.deepEqual(results, {})
      })
    })
  })

  describe('#toJSON', function() {
    it('returns an array of [token, docIds]', function() {
      let idx = new Index()
      idx.addDoc({id: 1, text: 'bob is fishing'})
      idx.addDoc({id: 2, text: 'hunting and fishing'})
      let json = idx.toJSON()
      assert.deepEqual(json, [
        ['bob', [1]],
        ['fish', [1, 2]],
        ['hunt', [2]]
      ])
    })
  })

  describe('constructor', function() {
    it('loads json', function() {
      let idx = new Index()
      idx.addDoc({id: 1, text: 'bob is fishing'})
      idx.addDoc({id: 2, text: 'hunting and fishing'})
      let json = idx.toJSON()
      let newIdx = new Index(json)
      assert.deepEqual(json, newIdx.toJSON())
    })
  })

  describe('tokenize', function() {
    it('returns a unique array', function() {
      let tokens = Index.tokenize('test test')
      assert.deepEqual(tokens, ['test'])
    })
  })
})
