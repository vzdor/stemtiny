'use strict';

let assert = require('assert')
let IrregularVerbs = require('../irregular_verbs')

describe('IrregularVerbs', function() {
  describe('get', function() {
    it('returns verb', function() {
      assert.equal(IrregularVerbs.get('ate'), 'eat')
    })

    context('with an unknown word', function() {
      it('returns null', function() {
        assert.equal(IrregularVerbs.get('blah'), null)
      })

      it('returns null for an object method', function() {
        assert.equal(IrregularVerbs.get('get'), null)
      })
    })
  })
})
