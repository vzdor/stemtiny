'use strict';

let assert = require('assert')
let AsyncIndex = require('../async_index')

class Tokdoc {
  constructor({token, docIds = []}) {
    this.docIds = docIds
    this.token = token
  }
}

describe('AsyncIndex', function() {
  describe('#find', function() {
    it('resolves with index like results', function() {
      class Dummy extends Tokdoc {
        static findAll(tokens) {
          return new Promise((resolve) => {
            resolve([
              new Tokdoc({token: tokens[0], docIds: [1, 2]}),
              new Tokdoc({token: tokens[1], docIds: [2]})
            ])
          })
        }
      }
      let index = new AsyncIndex({Tokdoc: Dummy})
      return index.find('hello world').then((results) => {
        assert.deepEqual(results, {
          1: ['hello'],
          2: ['hello', 'world']
        })
      })
    })

    context('with partially loaded tokens', function() {
      it('resolves with all docs', function() {
        class Dummy extends Tokdoc {
          static findAll(tokens) {
            return new Promise((resolve) => {
              resolve([
                new Tokdoc({token: tokens[0], docIds: [1, 2]}),
              ])
            })
          }
        }
        let tokdocs = [
          ['world', new Tokdoc({token: 'world', docIds: [1]})]
        ]
        let index = new AsyncIndex({Tokdoc: Dummy, tokdocs: tokdocs})
        return index.find('hello world').then((results) => {
          assert.deepEqual(results, {
            1: ['hello', 'world'],
            2: ['hello']
          })
        })
      })

    })
  })

  describe('#addDoc', function() {
    it('adds tokdocs using addDocid', function() {
      class Dummy extends Tokdoc {
        addDocid(id) {
          this.docIds.push(id)
        }
      }
      let index = new AsyncIndex({Tokdoc: Dummy})
      index.addDoc({id: 1, text: 'hello world'})
      assert.equal(index.tokdocs.length, 2)
      assert.deepEqual(index.docs('world'), [1])
      assert.deepEqual(index.docs('hello'), [1])
    })
  })
})
