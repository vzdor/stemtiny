'use strict';

let assert = require('assert')
let Tokenizer = require('../tokenizer')

describe('Tokenizer', function() {
  describe('tokenize', function() {
    it('returns an array of tokens', function() {
      let array = Tokenizer.tokenize(" my   dog hasn't any fleas.  ")
      assert.deepEqual(array, ['my', 'dog', 'hasn', 't', 'any', 'fleas'])
    })
  })
})
