'use strict';

let assert = require('assert')

let fs = require('fs')
let path = require('path')

let Index = require('../index')
let FsIndexBucket = require('../fs_index_bucket')

const IndexDir = '/tmp/stemtiny-test'

var deleteDir = function(dir) {
  if (fs.existsSync(dir)) {
    fs.readdirSync(dir).forEach(function(file, index){
      fs.unlinkSync(path.join(dir, file))
    })
    fs.rmdirSync(dir)
  }
}

describe('FsIndexBucket', function() {
  beforeEach(function() {
    deleteDir(IndexDir)
    fs.mkdirSync(IndexDir)
  })

  describe('#loadIndex', function() {
    it('returns a promise', function() {
      let fsIdxBucket = new FsIndexBucket({indexDir: IndexDir})
      let promise = fsIdxBucket.loadIndex('b-0')
          .then(function() {})
          .catch(function() {})
      assert(promise instanceof Promise)
    })

    it('resolves', function() {
      let fsIdxBucket = new FsIndexBucket({indexDir: IndexDir})
      return fsIdxBucket.loadIndex('b-0').then(function(index) {
        assert(true)
      }).catch(function() { assert(false) })
    })

    it('resolves with an index file', function() {
      let idx1 = new Index()
      idx1.addDoc({id: 1, text: 'bob'})
      let serialized = JSON.stringify(idx1)
      fs.writeFileSync(path.resolve(IndexDir, 'b-0'), serialized)
      let fsIdxBucket = new FsIndexBucket({indexDir: IndexDir})

      // We know it is a promise. It will timeout if there is no call
      // to resolve or reject.

      return fsIdxBucket.loadIndex('b-0').then(function(index) {
        assert.deepEqual(index.docs('bob'), [1])
      })
    })

    context('with a missing file', function() {
      it('resolves with a new index', function() {
        let fsIdxBucket = new FsIndexBucket({indexDir: IndexDir})
        return fsIdxBucket.loadIndex('b-0').then(function(index) {
          assert(index)
        })
      })
    })

    context('when called multiple times with the same index id', function() {
      it('resolves with the same index object', function() {
        let fsIdxBucket = new FsIndexBucket({indexDir: IndexDir})
        return Promise.all([fsIdxBucket.loadIndex('b-0'),
                            fsIdxBucket.loadIndex('b-0')])
          .then(function(indexes) {
            assert.equal(indexes.length, 2)
            assert.equal(indexes[0], indexes[1])
          })
      })
    })
  })

})
