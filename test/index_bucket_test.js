'use strict';

let assert = require('assert')
let Index = require('../index')
let Bucket = require('../bucket')
let IndexBucket = require('../index_bucket')

class DummyBucket extends Bucket {
  constructor(token) {
    super(token, {'*' : 2})
  }

  next() {
    return this.shift()
  }
}

class DummyIndexBucket extends IndexBucket {
  constructor(options = {}) {
    super(Object.assign({Bucket: DummyBucket}, options))
    this.loadedIndexes = {}
  }

  loadIndex(indexId) {
    return new Promise((resolve, reject) => {
      let index = this.loadedIndexes[indexId]
      if (! index)
        index = this.loadedIndexes[indexId] = new Index()
      resolve(index)
    })
  }
}

describe('IndexBucket', function() {
  describe('#addDoc', function() {
    it('returns a promise', function() {
      let idxBucket = new DummyIndexBucket()
      let promise = idxBucket.addDoc({id: 1, text: 'bob'})
      assert(promise instanceof Promise)
    })

    it('resolves', function() {
      let idxBucket = new DummyIndexBucket()
      return idxBucket.addDoc({id: 1, text: 'bob'}).then(function() {
        assert(true)
      }).catch(function() {
        assert(false)
      })
    })

    context('with empty index', function() {
      it('adds doc and resolves with array of [indexId, index] pairs', function() {
        let idxBucket = new DummyIndexBucket()
        let doc = {
          id: 1,
          text: 'bob is fishing'
        }
        return idxBucket.addDoc(doc).then(function(indexPairs) {
          assert.equal(indexPairs.length, 2)
          let indexMap = new Map(indexPairs)
          let idx = indexMap.get('b-1')
          assert(idx)
          assert.deepEqual(idx.docs('bob'), [1])
        })
      })

      it('adds the same token to the same index', function() {
        let bucket = new DummyBucket('test')
        class SpecialBucket {
          next() {
            return bucket.next()
          }

          get length() {
            return bucket.length
          }
        }
        let idxBucket = new DummyIndexBucket({Bucket: SpecialBucket})
        let doc = {
          id: 1,
          text: 'test test'
        }
        return idxBucket.addDoc(doc).then(function(indexPairs) {
          let indexMap = new Map(indexPairs)
          assert.equal(indexMap.size, 1)
        })
      })
    })

    context('with existing index', function() {
      it('adds document', function() {
        let idxBucket = new DummyIndexBucket()
        let idx = new Index()
        idx.addDoc({id: 1, text: 'bob'})
        idxBucket.loadedIndexes['b-0'] = idx
        let newDoc = {
          id: 2,
          text: 'bob'
        }
        return idxBucket.addDoc(newDoc).then(function(indexPairs) {
          assert.equal(indexPairs.length, 1)
          assert.deepEqual(idx.docs('bob').sort(), [1, 2])
        })
      })
    })

    context('when loadIndex fails', function() {
      it('rejects', function() {
        let idxBucket = new DummyIndexBucket()
        idxBucket.loadIndex = function(indexId) {
          return new Promise((resolve, reject) => reject('e'))
        }
        return idxBucket.addDoc({id: 1, text: 'bob'}).then(function() {
          assert(false)
        }).catch(function(x) {
          assert.equal(x, 'e')
        })
      })
    })
  })

  describe('#find', function() {
    it('returns a promise')

    it('resolves')

    it('resolves with relevant docs in index format', function() {
      let idx1 = new Index()
      idx1.addDoc({id: 1, text: 'bob'})
      let idx2 = new Index()
      idx2.addDoc({id: 1, text: 'fish'})
      idx2.addDoc({id: 3, text: 'fish'})
      idx2.addDoc({id: 2, text: 'free'})
      let idxBucket = new DummyIndexBucket()
      idxBucket.loadedIndexes = {
        'b-0': idx1,
        'f-1': idx2
      }
      return idxBucket.find('bob is fishing.').then(function(results) {
        assert.deepEqual(results, {
          1: ['bob', 'fish'],
          3: ['fish']
        })
      })
    })

    context('with empty indexes', function() {
      it('resolves with a blank object', function() {
        let idxBucket = new DummyIndexBucket()
        return idxBucket.find('bob blah.').then(function(results) {
          assert.deepEqual(results, {})
        })
      })
    })

  })
})
