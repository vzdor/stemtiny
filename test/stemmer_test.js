'use strict';

let assert = require('assert')
let stemmer = require('../stemmer')

describe('stemmer', function() {
  it('returns stem', function() {
    assert.equal(stemmer('fish'), 'fish')
    assert.equal(stemmer('fishing'), 'fish')
    assert.equal(stemmer('fished'), 'fish')
  })
})
